# A C++ Multithreaded HFT Exchange Stack and Trading Stack conforming to NASDAQ ITCH/OUCH Standards

In this project, we have one Trading Stack, one Exchange, one Tickerplant and one Gateway. As of 4/10/2023, the Exchange and the Trading Stack both have limited functionaility in the types of orders they support and the complexity of the algorithms. Only the OME and the Exchange are used to demonstrate functionailty. 

[**To Source Code Documentation**](documentation/SRC_DOCUMENTATION.md)

## Table of Contents

- [A C++ Multithreaded HFT Exchange Stack confiming to NASDAQ ITCH/OUCH Standards](#a-c-multithreaded-hft-exchange-stack-confiming-to-nasdaq-itchouch-standards)
  - [Table of Contents](#table-of-contents)
  - [Team](#team)
    - [**Benjamin Nguyen (Group Lead)**](#benjamin-nguyen-group-lead)
    - [**Justin Kuhn**](#justin-kuhn)
    - [**Yuyang Zhao**](#yuyang-zhao)
  - [Project Description](#project-description)
  - [Technologies](#technologies)
  - [Components](#components)
    - [Exchange](#exchange)
    - [HFT](#hft)
  - [Git Repo Layout](#git-repo-layout)
  - [Instructions for using the project](#instructions-for-using-the-project)
    - [**Project Dependencies:**](#project-dependencies)
    - [1) Git checkout](#1-git-checkout)
    - [2) Create the VMs](#2-create-the-vms)
    - [3) SSH into VMs](#3-ssh-into-vms)
      - [SSH with command line](#ssh-with-command-line)
      - [SSH with VSCode](#ssh-with-vscode)
    - [4) Running the program](#4-running-the-program)
  - [Testing](#testing)
    - [Running the tests](#running-the-tests)
  - [Project Results](#project-results)
  - [Screenshots and Example Outputs](#screenshots-and-example-outputs)
  - [Postmortem Summary](#postmortem-summary)
  - [TODOs](#todos)

## Team

### **Benjamin Nguyen (Group Lead)**

Hi! I currently an Undergraduate at UIUC graduating in December 2022. I am the CTO of [Quant Illinois at UIUC](https://www.quantillinois.com/) and am very interested in the High Frequency Trading space, specifically in regards to Software Engineering, designing systems, and optimization. I want to explore more of different market microstructures and help optimize trading algorithms for specific applications. I am planning on pursuing an MCS at UIUC in Fall 2023.

<img src=https://brand.linkedin.com/content/dam/me/business/en-us/amp/brand-site/v2/bg/LI-Bug.svg.original.svg alt="isolated" width="24"/> [www.linkedin.com/in/benjaminkhacnguyen](www.linkedin.com/in/benjaminkhacnguyen)

<img src=https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png alt="isolated" width="20"/> [www.github.com/ben44496](www.github.com/ben44496)

### **Justin Kuhn**
Hey there! I'm Justin, and I have a love for low-level programming and finance-related topics. My interest in finance was sparked back in 2013 when I started my first cryptocurrency mining pool(Dogecoin). Ever since then, I have been driven to study the intersection of computer science and finance. 

I have a background in computer science and am always looking for ways to optimize coding and design processes. Working on projects like the high frequency trading stack, I'm able to explore my interests in both finance and computer science while adding to my understanding of high-frequency trading, software engineering, and optimization.

<img src=https://brand.linkedin.com/content/dam/me/business/en-us/amp/brand-site/v2/bg/LI-Bug.svg.original.svg alt="isolated" width="24"/> [www.linkedin.com/in/justin-lkuhn](www.linkedin.com/in/justin-lkuhn)


### **Yuyang Zhao**
An Undergraduate Computer Engineer graduating December 2022 or May 2023. I have done some keyboard firmware work using [QMK](qmk.fm), and am generally thinking about working on embedded devices or low level programming.

<img src=https://brand.linkedin.com/content/dam/me/business/en-us/amp/brand-site/v2/bg/LI-Bug.svg.original.svg alt="isolated" width="24"/> [www.linkedin.com/in/chaoyuyang/](www.linkedin.com/in/chaoyuyang/)

<img src=https://github.githubassets.com/images/modules/logos_page/GitHub-Mark.png alt="isolated" width="20"/> [www.github.com/peepeetee](www.github.com/peepeetee)

## Project Description

This project was done as part of IE 421 - High Frequency Trading, taught by [Professor David Lariviere](https://davidl.web.illinois.edu/) at the University of Illinois at Urbana-Champaign.

This Exchange Part is divided into three parts: the Order Matching Engine (OME), the Gateway, and the Tickerplant. For the purposes of demonstration, we are only using the OME. Around 90% of the code is located in the OME repository due to the nature of the OME being where the trades are being executed, with the Gateway performing the networking switching depending on the stock symbol of the packet. We have designed the Exchange to purposely use the NASDAQ [ITCHV5.0](https://www.nasdaqtrader.com/content/technicalsupport/specifications/dataproducts/NQTVITCHSpecification.pdf) and [OUCHv4.2](http://www.nasdaqtrader.com/content/technicalsupport/specifications/tradingproducts/ouch4.2.pdf) specifications for orders to the exchange as well as market data feed. The idea behind this is both to gain familiarity with this protocol, as well as to provide market-by-order (level 3) data feed as part of our deep dive into HFT technologies.

The Order Matching Engine (OME) is the "brain" of the Exchange. Order Entry (in NASDAQ OUCH format) is forwarded from the Gateway to the correct OME that has the traded stock symbol. These messages are then parsed, updated in the Orderbook, and then sends the Outbound OUCH and ITCH messages to the corresponding Gateway and Tickerplant respectively. The OME contains the OME code, the Orderbook book building, code for parsing structs to and from unsigned char buffer arrays from network IO, and struct information. The OME is a TCP server that maintains connections to the Gateways. The OME is also treated as a UDP server that maintains connections to the Tickerplant (not implemented yet). There is also both behaviour driven testing and unit testing available for the OME code that uses the C++ [Catch2](https://github.com/catchorg/Catch2) Testing Framework which is expanded upon in the [Technologies](#technologies) and [Testing](#testing) sections.

The Gateway and Tickerplants are very lightweight TCP and UDP servers, respectively, and they maintain a connection between both Market Participants and the Order Matching Engines. The Gateway has additional functionality to send the message to the correct OME based on the stock symbol. It will also throw away any messages that are not for any of the Market Participants on its network, and this is done through the "MPID" functionality supplied by the orders. As of 12/14/2022, the only difference between the message sent by the OME and the one send to the Market Participant from the gateway is that the gateway will strip the mpid attribute at the end of the message.

The Trading Stack is responsible for executing and managing the trading strategies. It is equipped with a basic backtesting capability that allows users to test and evaluate the performance of their strategies on historical data. This feature allows for the refining and optimization of trading algorithms before deploying them in real-time situations. The trading stack employs a simple moving average (SMA) strategy, to generate buy and sell signals based on the trends observed in the moving averages of the asset prices.

To handle the market data, the trading stack is designed to parse NASDAQ ITCH daily market data streams directly. By parsing the ITCH messages, the trading stack can construct a real-time representation of the order book, which is crucial for making informed trading decisions.

The trading stack is also equipped with a TCP server for sending inbound OUCH messages, enabling efficient communication with the Order Matching Engine (OME). In addition, it acts as a client for receiving outbound OUCH messages and ITCH multicast updates. 

There is a significant difference between how the OME and the trading stack design the message structures. The trading stack uses packed structures, while the OME does not. The OME stuctures also heavily rely on inheritance for functionality, while the trading stack does not. 

## Technologies

See [TECHNOLOGIES.md](documentation/TECHNOLOGIES.md)

## Components

### Exchange

See [EXCHANGE_COMPONENTS.md](documentation/EXCHANGE_COMPONENTS.md) // TODO: Finish

### HFT

See [HFT_COMPONENTS.md](documentation/HFT_COMPONENTS.md)

## Git Repo Layout

- [documentation/](documentation/)
  - Contains longer Markdown files with project-specific information. Documents are linked to from this README as well.   
- [.gitignore](.gitignore)
  - Files to ignore for Git. Usually just the `.vagrant/` and associated development specific files.
- [.markdownlint.json](.markdownlint.json)
  - JSON config file for [Markdown All in One VSCode extension by Yu Zhang](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one)
- [README.md](README.md)
- [Vagrantfile](Vagrantfile)
  - Vagrant provisioning setup script for the Trading Stack


## Instructions for using the project

All the code will be instantiated within each container and will automatically git checkout the main branch of the respective code.

### **Project Dependencies:**

- Vagrant v2.3.0

> Note: To check what versions we are using for our tools please see [TECHNOLOGIES.md](documentation/TECHNOLOGIES.md).

### 1) Git checkout

1. Clone the code to your local machine.
   1. Open a terminal
   2. Navigate to your desired folder with `cd`
   3. (For the Exchange) In your terminal, run `git clone https://gitlab.engr.illinois.edu/ie421_high_frequency_trading_fall_2022/ie421_hft_fall_2022_group_04/order-book-generation.git`
   4. (For the Stack) In your terminal, run `git clone https://gitlab.engr.illinois.edu/ie421_high_frequency_trading_fall_2022/ie421_hft_fall_2022_group_04/hft-stack.git`

### 2) Create the VMs

1. (For the Exchange) Continue in your terminal, run `cd order-book-generation/`
2. Run `vagrant up --provision`.
3. Wait for the provisioning to finish. Watch the output for errors. If there are any errors, please feel free to make an issue on our repository or try and debug it yourself.
4. Run `vagrant status` to make sure that you have 1 VM provisioned named `order-matching engine` and it is "running."
5. (For the Stack) Continue in your terminal, run `cd hft-stack/`
6. Follow steps 2-4 again. This VM provisioned will be named `client`


Note: Both VMs will then be provisioned and automatically clone the repositories at `/home/vagrant` which is your user directory `~/`.

### 3) SSH into VMs

There are two ways to SSH into your VM. You can either do it through command line or do it within VSCode which is the preferred method.

#### SSH with command line

1. Continue in your terminal, and run `vagrant ssh [VM Name]`, where `[VM Name]` is the name of the VM (ie. `client`, `order-matching-engine`)
2. You will then be logged into the `vagrant` user, and your terminal should look something like `vagrant@vagrant:~/ $`
   1. If you for some reason need to do any changes on the repository (say running `cmake` or `make`), you will need superuser access because we ran these commands during the provisioning. If you need this type of access, just type in `sudo su`.

#### SSH with VSCode

0. Make sure to have the "Remote Explorer" VSCode extension, and navigate to the "Remote" targets in the dropdown menu.
1. Continue in your terminal, and run `vagrant ssh-config`
2. Copy the output with right click > copy
3. Click the Settings icon next to "SSH" > and choose your user's `[USER]/.ssh/config` file.
4. Paste the contents (except for any line with the prefix "`==> vagrant`") at the top of the file.
5. Click the Refresh icon next to "Remote" in VSCode's Remote Explorer
6. You should now see 2 new SSH targets appear, which should be named `client`, and `order-matching-engine`
7. Click on the new window icon next to whichever target you would like to open and it should open a new VSCode window and log in for you.
   1. If you are prompted to choose the OS, choose Linux.
   2. Furthermore, if you are doing any development work or looking through the repository, you should probably choose `Open Folder... > Choose ~/[REPOSITORY NAME] > Ok` to open the VSCode window exactly to the repository.

### 4) Running the program

The program should already be running. However, if you need to run any specific program please make sure to SSH into the correct VM and run the executable in the repository's `build/` directory with superuser permission through `sudo su`.

You should be able to connect to the VM and send a TCP message in OUCH format and receive a response depending on the type of message. Please look through the example main code in the repositories to get a handle how to exchange messages and receive them. Also you should check the IPs of the VMs and the port to make sure you are connected correctly for TCP.

As of 4/10/2023, please note the following VM settings:

- `client` - IPv4: 192.168.56.1, 
- `order-matching-engine` - IPv4: 192.168.56.2
- `gateway1` - IPv4: 192.168.64.1
- `tp1` - IPv4: 192.168.63.1

## Testing

Note: Formal Testing is currently only functional on the `order-matching-engine`. Adding networking to the OME broke the tests, need to fix. 

To run test, you must follow the steps outlined in [Instructions for using the project](#instructions-for-using-the-project) and correctly SSH into the VM with the repository you want to run the test on, and you may need to change to the superuser by running `sudo su` in the terminal (you don't have to do this unless you get a permission denied error). The steps for testing are identical between the different repositories and is outlined below.

### Running the tests

1. SSH into the Machine (`vagrant ssh` or through VSCode)
2. Navigate to the repository's build folder (`~/[REPOSITORY NAME]/build`)
3. Run `./test`

If all tests are passing, you should see something akin to (following output from `order-matching-engine`):

```txt
===============================================================================
All tests passed (227 assertions in 20 test cases)
```

## Project Results

We started on the project fairly early back in September. We originally envisioned a simulated trader/exchange ecosystem with multiple traders acting on an exchange. However, for simplicity, we decided to focus on making sure that one exchange can connect to one trading stack. This allowed us to focus more on building features within the exchange and the HFT stack instead of dealing with the networking involved with multiple trading stacks. The project is currently (4/10/2023) in a barebones state. There are some message types in the ITCH and OUCH protocols that we do not yet support. However, I think our foundation is strong, and we would be able to support these types in the future. The project also took much longer than we expected. This was due to several factors, such as busy schedules and overly ambitious project goals.


## Screenshots and Example Outputs

See [EXAMPLE_OUTPUT.md](documentation/EXAMPLE_OUTPUT.md)

## Postmortem Summary

See [POST_MORTEM.md](documentation/POST_MORTEM.md)  // TODO

## TODOs

See [MISSING_COMPONENTS.md](documentation/MISSING_COMPONENTS.md) // TODO
