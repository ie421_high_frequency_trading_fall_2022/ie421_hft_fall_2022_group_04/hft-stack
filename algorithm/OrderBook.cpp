#include "OrderBook.h"

time_t convert_timestamp(const unsigned char timestamp[6]);



void OrderBook::add_order(const ITCH::AddOrderMessageWithMPID &order_message) {
    Order new_order;
    new_order.order_reference_number = order_message.order_reference_number;
    new_order.shares = order_message.shares;
    new_order.price = order_message.price;
    new_order.attribution = std::string(order_message.attribution, 4);

    if (order_message.buy_sell_indicator == 'B') {
        buy_side[new_order.order_reference_number] = new_order;
    } else if (order_message.buy_sell_indicator == 'S') {
        sell_side[new_order.order_reference_number] = new_order;
    } else {
        // Invalid buy/sell indicator. Handle error as needed.
    }
}





void OrderBook::execute_order(const ITCH::OrderExecutedMessage &execute_message) {
    uint64_t order_reference_number = execute_message.order_reference_number;
    uint32_t executed_shares = execute_message.executed_shares;

    auto buy_order_it = buy_side.find(order_reference_number);
    auto sell_order_it = sell_side.find(order_reference_number);

    if (buy_order_it != buy_side.end()) {
        if (buy_order_it->second.shares > executed_shares) {
            buy_order_it->second.shares -= executed_shares;
        } else {
            buy_side.erase(buy_order_it);
        }
    } else if (sell_order_it != sell_side.end()) {
        if (sell_order_it->second.shares > executed_shares) {
            sell_order_it->second.shares -= executed_shares;
        } else {
            sell_side.erase(sell_order_it);
        }
    } else {
        // Order not found. Handle error as needed.
    }
}





void OrderBook::cancel_order(const ITCH::OrderCancelMessage &cancel_message) {
    uint64_t order_reference_number = cancel_message.order_reference_number;
    uint32_t cancelled_shares = cancel_message.cancelled_shares;

    auto buy_order_it = buy_side.find(order_reference_number);
    auto sell_order_it = sell_side.find(order_reference_number);

    if (buy_order_it != buy_side.end()) {
        if (buy_order_it->second.shares > cancelled_shares) {
            buy_order_it->second.shares -= cancelled_shares;
        } else {
            buy_side.erase(buy_order_it);
        }
    } else if (sell_order_it != sell_side.end()) {
        if (sell_order_it->second.shares > cancelled_shares) {
            sell_order_it->second.shares -= cancelled_shares;
        } else {
            sell_side.erase(sell_order_it);
        }
    } else {
        std::cout << "Order not found: " << order_reference_number << std::endl;
    }
}




void OrderBook::delete_order(const ITCH::OrderDeleteMessage &delete_message) {
    uint64_t order_reference_number = delete_message.order_reference_number;

    auto buy_order_it = buy_side.find(order_reference_number);
    auto sell_order_it = sell_side.find(order_reference_number);

    if (buy_order_it != buy_side.end()) {
        buy_side.erase(buy_order_it);
    } else if (sell_order_it != sell_side.end()) {
        sell_side.erase(sell_order_it);
    } else {
        // Order not found. Handle error as needed.
    }
}


void OrderBook::replace_order(const ITCH::OrderReplaceMessage &replace_message) {
    uint64_t old_order_reference_number = replace_message.order_reference_number;
    uint64_t new_order_reference_number = replace_message.new_order_reference_number;
    uint32_t new_shares = replace_message.shares;
    uint32_t new_price = replace_message.price;

    auto buy_order_it = buy_side.find(old_order_reference_number);
    auto sell_order_it = sell_side.find(old_order_reference_number);

    if (buy_order_it != buy_side.end()) {
        Order new_order = buy_order_it->second;
        new_order.order_reference_number = new_order_reference_number;
        new_order.shares = new_shares;
        new_order.price = new_price;
        buy_side.erase(buy_order_it);
        buy_side[new_order_reference_number] = new_order;
    } else if (sell_order_it != sell_side.end()) {
        Order new_order = sell_order_it->second;
        new_order.order_reference_number = new_order_reference_number;
        new_order.shares = new_shares;
        new_order.price = new_price;
        sell_side.erase(sell_order_it);
        sell_side[new_order_reference_number] = new_order;
    } else {
        // Order not found. Handle error as needed.
    }
}


void OrderBook::print_order_book() const {
    std::cout << "PRINTING ORDER BOOK" << std::endl;
    std::cout << "Buy Side:" << std::endl;
    std::cout << std::setw(20) << "Order Reference" << std::setw(10) << "Shares" << std::setw(10) << "Price" << std::endl;
    for (const auto &buy_order : buy_side) {
        std::cout << std::setw(20) << buy_order.first << std::setw(10) << buy_order.second.shares << std::setw(10) << buy_order.second.price << std::endl;
    }

    std::cout << std::endl << "Sell Side:" << std::endl;
    std::cout << std::setw(20) << "Order Reference" << std::setw(10) << "Shares" << std::setw(10) << "Price" << std::endl;
    for (const auto &sell_order : sell_side) {
        std::cout << std::setw(20) << sell_order.first << std::setw(10) << sell_order.second.shares << std::setw(10) << sell_order.second.price << std::endl;
    }
}



void OrderBooks::print_all_order_books() const{
    for (const auto& book_pair : this->books) {
        uint16_t stock_locate = book_pair.first;
        const OrderBook& order_book = book_pair.second;

        std::cout << "Printing order book for stock locate: " << stock_locate << std::endl;
        order_book.print_order_book();
        std::cout << std::endl;
    }
}





time_t convert_timestamp(const unsigned char timestamp[6]) {
    uint64_t nanoseconds = 0;
    for (int i = 0; i < 6; i++) {
        nanoseconds = (nanoseconds << 8) | timestamp[i];
    }
    time_t seconds = nanoseconds / 1000000000;
    return seconds;
}
