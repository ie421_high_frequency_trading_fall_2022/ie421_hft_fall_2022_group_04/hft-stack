#include "algorithm.h"
#include <iostream>

TradingStrategy::TradingStrategy(OrderBooks& books) : books(books) {}

InboundOUCH::EnterOrderMessage* TradingStrategy::analyze_market_and_make_decisions() {
    // std::cout << "TRADING" <<std::endl;

    for (auto &book : books.books) 
    {
        double buy_sma = calculate_sma(book.second.buy_side, 5);
        double sell_sma = calculate_sma(book.second.sell_side, 5);

        if (buy_sma == -1 || sell_sma == -1) 
        {
            continue; // Not enough data to calculate SMA
        }
        

        if (buy_sma > sell_sma) 
        {
            std::cout << "BUY SIGNAL" << std::endl;
            // Buy signal, place a buy order
            // Use the `EnterOrderMessage` struct from the InboundOUCH namespace and other necessary functions
            InboundOUCH::EnterOrderMessage *enter_order_message = new InboundOUCH::EnterOrderMessage;
            enter_order_message->type = 'O'; // Enter Order message type

            // Fill in the required fields for the message
            // For example: order_token, buy_sell_indicator, shares, stock, price, time_in_force, etc.
            // Make sure to use appropriate values for your specific use case

            char order_token[14];
            snprintf(order_token, sizeof(order_token), "%013d", rand());
            order_token[13] = '\0'; // Add null terminator to the 14th character
            // Copy the order token to the EnterOrderMessage struct
            strncpy((char*)enter_order_message->order_token, order_token, 14);
            enter_order_message->buy_sell_indicator = 'B'; // Buy order
            enter_order_message->shares = 100; // Number of shares to buy
            strncpy((char*)enter_order_message->stock, "AAPL\0\0\0\0", 8); // Stock symbol (left-justified, padded with spaces)
            enter_order_message->price = static_cast<uint32_t>(sell_sma * 10000); // Price in integer format (e.g., 123456 for $12.3456)
            enter_order_message->time_in_force = 30000; // Time in force (in seconds, e.g., 30 seconds)
            strncpy((char*)enter_order_message->firm, "ABC\0", 4); // Firm identifier
            enter_order_message->display = 'Y'; // Displayed order
            enter_order_message->capacity = 'A'; // Agency capacity
            enter_order_message->intermarket_sweep_eligibility = 'Y'; // Intermarket sweep eligible
            enter_order_message->minimum_quantity = 1; // Minimum quantity
            enter_order_message->cross_type = 'N'; // Not a cross order
            enter_order_message->customer_type = 'C'; // Customer type (e.g., retail)
            return enter_order_message;
        }
        else if (buy_sma < sell_sma) 
        {
            std::cout << "SELL SIGNAL" << std::endl;
            InboundOUCH::EnterOrderMessage *enter_order_message =  new InboundOUCH::EnterOrderMessage;
            enter_order_message->type = 'O'; // Enter Order message type
            // Sell signal, place a sell order
            char order_token[14];
            snprintf(order_token, sizeof(order_token), "%013d", rand());
            order_token[13] = '\0'; // Add null terminator to the 14th character
            // Copy the order token to the EnterOrderMessage struct
            strncpy((char*)enter_order_message->order_token, order_token, 14);
            // strncpy((char*)enter_order_message->order_token, "9876543210987", 14); // A unique order token
            enter_order_message->buy_sell_indicator = 'S'; // Sell order
            enter_order_message->shares = 100; // Number of shares to sell
            strncpy((char*)enter_order_message->stock, "AAPL\0\0\0\0", 8); // Stock symbol (left-justified, padded with spaces)
            enter_order_message->price = static_cast<uint32_t>(buy_sma * 10000); // Price in integer format (e.g., 123456 for $12.3456)
            enter_order_message->time_in_force = 30000; // Time in force (in seconds, e.g., 30 seconds)
            strncpy((char*)enter_order_message->firm, "ABC\0", 4); // Firm identifier
            enter_order_message->display = 'Y'; // Displayed order
            enter_order_message->capacity = 'A'; // Agency capacity
            enter_order_message->intermarket_sweep_eligibility = 'Y'; // Intermarket sweep eligible
            enter_order_message->minimum_quantity = 1; // Minimum quantity
            enter_order_message->cross_type = 'N'; // Not a cross order
            enter_order_message->customer_type = 'C'; // Customer type (e.g., retail)
            return enter_order_message;
        }
    }
    return nullptr;
}

double TradingStrategy::calculate_sma(const std::map<uint64_t, OrderBook::Order>& orders, unsigned int period) {
    if (orders.size() < period) 
    {
        return -1; // Not enough data to calculate SMA
    }
    double sum = 0;
    unsigned int count = 0;
    for (auto it = orders.rbegin(); it != orders.rend() && count < period; ++it, ++count) 
    {
        sum += it->second.price;
    }

    return sum / period;
}

