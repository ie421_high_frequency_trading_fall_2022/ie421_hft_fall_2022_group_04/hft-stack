// #pragma once
#include "OrderBook.h"
#include "nasdaq_ouch_inbound.h"
#include <deque>
#include <numeric>
#include <cstring>

class TradingStrategy {
public:
    TradingStrategy(OrderBooks& books);
    InboundOUCH::EnterOrderMessage* analyze_market_and_make_decisions();

private:
    OrderBooks& books;
    double calculate_sma(const std::map<uint64_t, OrderBook::Order>& orders, unsigned int period);
};
