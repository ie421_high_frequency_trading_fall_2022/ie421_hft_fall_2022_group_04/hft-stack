#pragma once
#include <vector>
#include "OrderBook.h"
#include "nasdaq_ouch_inbound.h"

uint64_t StringHash(std::string str);

class Backtester {
public:
    Backtester(OrderBooks& books, std::vector<InboundOUCH::EnterOrderMessage*>& inbound_ouch_messages);
    void run_backtest();

private:
    OrderBooks& books;
    std::vector<InboundOUCH::EnterOrderMessage*>& inbound_ouch_messages;
    double process_trade(const InboundOUCH::EnterOrderMessage& message);
};
