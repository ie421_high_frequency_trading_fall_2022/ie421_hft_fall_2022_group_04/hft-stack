#include "Backtester.h"
#include <cassert>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <cstring>

uint64_t StringHash(std::string str) {
    std::hash<std::string> hash_fn;
    return hash_fn(str);
}

Backtester::Backtester(OrderBooks& books, std::vector<InboundOUCH::EnterOrderMessage*>& inbound_ouch_messages)
    : books(books), inbound_ouch_messages(inbound_ouch_messages) {}

void Backtester::run_backtest() {
    double total_profit = 0.0;
    unsigned int successful_trades = 0;
    unsigned int total_trades = 0;

    for (InboundOUCH::EnterOrderMessage* message : inbound_ouch_messages) {
        double trade_profit = process_trade(*message);
        if (trade_profit != 0) {
            total_profit += trade_profit;
            successful_trades++;
        }
        total_trades++;
    }

    std::cout << "Backtest Results: " << std::endl;
    std::cout << "Total Trades: " << total_trades << std::endl;
    std::cout << "Successful Trades: " << successful_trades << std::endl;
    std::cout << "Total Profit: $" << total_profit << std::endl;
}

double Backtester::process_trade(const InboundOUCH::EnterOrderMessage& message) {
    
    // uint16_t stock_locate = StringHash(std::string((char*)message.stock));
    // TODO HARD CODED NO-NO
    auto& book = books.books[9070];
    
    char side = message.buy_sell_indicator;
    uint32_t order_price = message.price;
    uint32_t order_shares = message.shares;

    double trade_profit = 0.0;

    if (side == 'B') {
        auto& sell_side = book.sell_side;
        auto it = sell_side.begin();
        while (it != sell_side.end() && order_shares > 0 && order_price >= it->second.price) {
            uint32_t executed_shares = std::min(order_shares, it->second.shares);
            trade_profit += executed_shares * (order_price - it->second.price) / 10000.0;
            order_shares -= executed_shares;
            it->second.shares -= executed_shares;

            if (it->second.shares == 0) {
                it = sell_side.erase(it);
            } else {
                ++it;
            }
        }
    } else if (side == 'S') {
        auto& buy_side = book.buy_side;
        auto it = buy_side.rbegin();
        while (it != buy_side.rend() && order_shares > 0 && order_price <= it->second.price) {
            uint32_t executed_shares = std::min(order_shares, it->second.shares);
            trade_profit += executed_shares * (it->second.price - order_price) / 10000.0;
            order_shares -= executed_shares;
            it->second.shares -= executed_shares;

            if (it->second.shares == 0) {
                it = decltype(it)(buy_side.erase(std::next(it).base()));
            } else {
                ++it;
            }
        }
    }
    return trade_profit;
}
