#pragma once
#include <queue>
#include <map>
#include <iostream>
#include <iomanip>
#include "nasdaq_itch.h"
#include "nasdaq_ouch_outbound.h"
#include <map>
#include <list>
#include <time.h>


class OrderBook {
public:
    OrderBook() = default;
    ~OrderBook() = default;

    void add_order(const ITCH::AddOrderMessageWithMPID &order_message);
    void execute_order(const ITCH::OrderExecutedMessage &execute_message);
    void cancel_order(const ITCH::OrderCancelMessage &cancel_message);
    void delete_order(const ITCH::OrderDeleteMessage &delete_message);
    void replace_order(const ITCH::OrderReplaceMessage &replace_message);
    void print_order_book() const;

    struct Order {
        uint64_t order_reference_number;
        uint32_t shares;
        uint32_t price;
        std::string attribution;
    };
    // std::map<uint64_t order_reference_number, Order> buy_side;
    std::map<uint64_t, Order> buy_side;

    // std::map<uint64_t order_reference_number, Order> sell_side;
    std::map<uint64_t, Order> sell_side;

};


class OrderBooks {

    // std::map<uint16_t, OrderBook> books;

public:
    // std::map<uint16_t stock_locate, OrderBook book> books;
    std::map<uint16_t, OrderBook> books;

    void print_all_order_books() const;


    void add_order(const ITCH::AddOrderMessageWithMPID &order_message) {
        uint16_t stock_locate = order_message.stock_locate;
        books[stock_locate].add_order(order_message);
    }

    void execute_order(const ITCH::OrderExecutedMessage &execute_message) {
        uint16_t stock_locate = execute_message.stock_locate;
        books[stock_locate].execute_order(execute_message);
    }

    void cancel_order(const ITCH::OrderCancelMessage &cancel_message) {
        uint16_t stock_locate = cancel_message.stock_locate;
        books[stock_locate].cancel_order(cancel_message);
    }

    void delete_order(const ITCH::OrderDeleteMessage &delete_message) {
        uint16_t stock_locate = delete_message.stock_locate;
        books[stock_locate].delete_order(delete_message);
    }
    
    void replace_order(const ITCH::OrderReplaceMessage &replace_message) {
        uint16_t stock_locate = replace_message.stock_locate;
        books[stock_locate].replace_order(replace_message);
    }
};
