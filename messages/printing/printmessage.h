#pragma once
#include "printmessage.cpp"
#include "nasdaq_itch.h"
#include <bitset>


static void printMessage(SystemEventMessage *e);
static void printMessage(AddOrderMessageWithMPID *e);
static void printMessage(OrderExecutedMessage *e);
static void printMessage(OrderCancelMessage *e);
static void printMessage(OrderDeleteMessage *e);
static void printMessage(OrderReplaceMessage *e);
static void printCorrectMessage(ITCHMessage *e);
