#include <string>
#include <iostream>
#include "nasdaq_itch.h"
#include <bitset>

using namespace ITCH;

static void printMessage(SystemEventMessage *e);
static void printMessage(AddOrderMessageWithMPID *e);
static void printMessage(OrderExecutedMessage *e);
static void printMessage(OrderCancelMessage *e);
static void printMessage(OrderDeleteMessage *e);
static void printMessage(OrderReplaceMessage *e);


static void printCorrectMessage(ITCHMessage *e)
{
    if (e->type == 'S')
    {
        std::cerr << "PRI(NTING): " << e->type << std::endl;
        printMessage((SystemEventMessage *)e);
    }

    else if (e->type == 'F')
    {
        printMessage((AddOrderMessageWithMPID *)e);
    }

    else if (e->type == 'E')
    {
        printMessage((OrderExecutedMessageBody *)e);
    }

    else if (e->type == 'X')
    {
        printMessage((OrderCancelMessageBody *)e);
    }

    else if (e->type == 'D')
    {
        printMessage((OrderDeleteMessageBody *)e);
    }

    else if (e->type == 'U')
    {
        printMessage((OrderReplaceMessageBody *)e);
    }

}

static void printMessage(SystemEventMessage *e)
{

    // std::cout << sizeof(*e);
    std::cout << e->type << std::endl;
    std::cout << std::bitset<8>(e->stock_locate) << std::endl;
    std::cout << std::bitset<16>(e->stock_tracking) << std::endl;
    // std::cout << std::bitset<48>(e->timestamp) << std::endl;
    std::cout << e->event_code << std::endl;
    std::cout << "----------------" << std::endl;
}

static void printMessage(AddOrderMessageWithMPID *e)
{
    // std::cout << sizeof(*e);
    std::cout << e->type << std::endl;
    std::cout << std::bitset<16>(e->stock_locate) << std::endl;
    std::cout << std::bitset<16>(e->stock_tracking) << std::endl;
    // std::cout << std::bitset<48>(e->timestamp) << std::endl;
    std::cout << e->order_reference_number << std::endl;
    std::cout << std::bitset<8>(e->buy_sell_indicator) << std::endl;
    std::cout << e->shares << std::endl;
    std::cout << e->stock << std::endl;
    std::cout << e->price << std::endl;
    std::cout << e->attribution << std::endl;
    std::cout << "----------------" << std::endl;
}

static void printMessage(OrderExecutedMessage *e)
{
    // std::cout << sizeof(*e);
    std::cout << e->type << std::endl;
    std::cout << std::bitset<8>(e->stock_locate) << std::endl;
    std::cout << std::bitset<16>(e->stock_tracking) << std::endl;
    // std::cout << std::bitset<48>(e->timestamp) << std::endl;
    std::cout << e->order_reference_number << std::endl;
    std::cout << e->executed_shares << std::endl;
    std::cout << e->match_number << std::endl;
    std::cout << "----------------" << std::endl;
}


static void printMessage(OrderCancelMessage *e)
{
    // std::cout << sizeof(*e);
    std::cout << e->type << std::endl;
    std::cout << std::bitset<8>(e->stock_locate) << std::endl;
    std::cout << std::bitset<16>(e->stock_tracking) << std::endl;
    // std::cout << std::bitset<48>(e->timestamp) << std::endl;
    std::cout << e->order_reference_number << std::endl;
    std::cout << e->cancelled_shares << std::endl;
    std::cout << "----------------" << std::endl;
}

static void printMessage(OrderDeleteMessage *e)
{
    // std::cout << sizeof(*e);
    std::cout << e->type << std::endl;
    std::cout << std::bitset<8>(e->stock_locate) << std::endl;
    std::cout << std::bitset<16>(e->stock_tracking) << std::endl;
    // std::cout << std::bitset<48>(e->timestamp) << std::endl;
    std::cout << e->order_reference_number << std::endl;
    std::cout << "----------------" << std::endl;
}

static void printMessage(OrderReplaceMessage *e)
{
    // std::cout << sizeof(*e);
    std::cout << e->type << std::endl;
    std::cout << std::bitset<8>(e->stock_locate) << std::endl;
    std::cout << std::bitset<16>(e->stock_tracking) << std::endl;
    // std::cout << std::bitset<48>(e->timestamp) << std::endl;
    std::cout << e->order_reference_number << std::endl;
    std::cout << e->new_order_reference_number << std::endl;
    std::cout << e->shares << std::endl;
    std::cout << e->price << std::endl;
    std::cout << "----------------" << std::endl;
}
