#pragma once
#include <string>

namespace InboundOUCH {
    typedef struct __attribute__((__packed__))InboundOUCHMessage {
    unsigned char type;
    }InboundOUCHMessage;

    //"O"
    typedef struct __attribute__((__packed__))EnterOrderMessageBody : InboundOUCHMessage {
        //unsigned char const type = 'S' : 8; // 'S'
        unsigned char order_token[14];
        unsigned char buy_sell_indicator;
        uint32_t shares;
        unsigned char stock[8];
        uint32_t price;
        uint32_t time_in_force;
        unsigned char firm[4];
        unsigned char display;
        unsigned char capacity;
        unsigned char intermarket_sweep_eligibility;
        uint32_t minimum_quantity;
        unsigned char cross_type;
        unsigned char customer_type;
    } EnterOrderMessage;

    //"U"
    typedef struct __attribute__((__packed__))ReplaceOrderMessageBody : InboundOUCHMessage {
        unsigned char existing_order_token[14];
        unsigned char replacement_order_token[14];
        uint32_t shares;
        uint32_t price;
        uint32_t time_in_force;
        unsigned char display;
        unsigned char intermarket_sweep_eligibility;
        uint32_t minimum_quantity;
    } ReplaceOrderMessage;

    //"X"
    typedef struct __attribute__((__packed__))CancelOrderMessageBody : InboundOUCHMessage {
        unsigned char mpid[4];
        unsigned char order_token[14];
        uint32_t shares;
    } CancelOrderMessage;

    //"M"
    typedef struct __attribute__((__packed__))ModifyOrderMessageBody : InboundOUCHMessage {
        unsigned char order_token[14];
        unsigned char buy_sell_indicator;
        uint32_t shares;
    } ModifyOrderMessage;

    //"N"
    typedef struct __attribute__((__packed__))TradeNowMessageBody : InboundOUCHMessage {
        unsigned char order_token[14];
    } TradeNowMessage;
}
