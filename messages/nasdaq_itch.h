// // NASDAQ I*T*C*H 
// // https://www.nasdaqtrader.com/content/technicalsupport/specifications/dataproducts/NQTVITCHSpecification.pdf

#pragma once
#include <string>

// Bitfields used due to irregular integer size of 48 bits in some fields

namespace ITCH {
    enum sizes
    {
        SystemEventMessageSize = 12,
        StockDirectoryMessageSize = 39,
        StockTradingActionMessageSize = 25,
        RegShoRestrictionMessageSize = 20,
        MarketParticipantPositionMessageSize = 26,
        MarketWideCircuitBreakerDeclineLevelMessageSize = 35,
        MarketWideCircuitBreakerStatusMessageSize= 12,
        IPOQuotingPeriodUpdateMessageSize = 28,
        LULDAuctionCollarMessageSize = 35,
        OperationalHaltMessageSize = 21,
        AddOrderMessageSize = 36,
        AddOrderMessageWithMPIDSize = 40,
        OrderExecutedMessageSize = 31,
        OrderExecutedWithPriceMessageSize = 36,
        OrderCancelMessageSize = 23,
        OrderDeleteMessageSize = 19,
        OrderReplaceMessageSize = 35,
        TradeMessageSize = 44,
        CrossTradeMessageSize = 40,
        BrokenTradeMessageSize = 19,
        NetOrderImbalanceIndicatorMessageSize = 50,
        RetailPriceImprovementIndicatorMessageSize = 20,
    };

    

    typedef struct __attribute__((__packed__))ITCHMessage {
    unsigned char type;
    } ITCHMessage;

    typedef struct __attribute__((__packed__))SystemEventMessageBody : ITCHMessage {
        //unsigned char const type = 'S' : 8; // 'S'
        uint16_t stock_locate;
        uint16_t stock_tracking;
        unsigned char timestamp[6];
        unsigned char event_code;
    } SystemEventMessage;


    typedef struct __attribute__((__packed__))AddOrderMessageWithMPIDBody : ITCHMessage {
        // unsigned char const type = 'F' // 'F'
        uint16_t stock_locate;
        uint16_t stock_tracking;
        unsigned char timestamp [6];
        uint64_t order_reference_number;
        unsigned char buy_sell_indicator;
        uint32_t shares;
        uint64_t stock;
        // unsigned char stock[8];
        uint32_t price;
        uint32_t attribution;
    } AddOrderMessageWithMPID;


    typedef struct __attribute__((__packed__))OrderExecutedMessageBody : ITCHMessage {
        //unsigned char const type = 'E' : 8; // 'E'
        uint16_t stock_locate;
        uint16_t stock_tracking;
        unsigned char timestamp[6];
        uint64_t order_reference_number;
        uint32_t executed_shares;
        uint64_t match_number;
    } OrderExecutedMessage;

      typedef struct __attribute__((__packed__))OrderExecutedWithPriceMessageBody : ITCHMessage {
        //unsigned char const type = 'C' : 8; // 'C'
        uint16_t stock_locate;
        uint16_t stock_tracking;
        unsigned char timestamp[6];
        uint64_t order_reference_number;
        uint32_t executed_shares;
        uint64_t match_number;
        unsigned char printable;
        uint32_t execution_price;
    } OrderExecutedWithPriceMessage;

    typedef struct __attribute__((__packed__))OrderCancelMessageBody : ITCHMessage {
        //unsigned char const type = 'X' : 8; // 'X'
        uint16_t stock_locate;
        uint16_t stock_tracking;
        unsigned char timestamp[6];
        uint64_t order_reference_number;
        uint32_t cancelled_shares;
    } OrderCancelMessage;

    typedef struct __attribute__((__packed__))OrderDeleteMessageBody : ITCHMessage {
        //unsigned char const type = 'D' : 8; // 'D'
        uint16_t stock_locate;
        uint16_t stock_tracking;
        unsigned char timestamp[6];
        uint64_t order_reference_number;
    } OrderDeleteMessage;

    typedef struct __attribute__((__packed__))OrderReplaceMessageBody : ITCHMessage {
        //unsigned char const type = 'U' : 8; // 'U'
        uint16_t stock_locate;
        uint16_t stock_tracking;
        unsigned char timestamp[6];
        uint64_t order_reference_number;
        uint64_t new_order_reference_number;
        uint32_t shares;
        uint32_t price;
    } OrderReplaceMessage;

}


