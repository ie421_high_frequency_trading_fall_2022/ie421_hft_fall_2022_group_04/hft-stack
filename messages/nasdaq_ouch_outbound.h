
#pragma once
#include <string>
namespace OutboundOUCH {
    typedef struct __attribute__((__packed__))OutboundOUCHMessage {
    unsigned char type;
    } OutboundOUCHMessage;


    //"S"
    typedef struct __attribute__((__packed__))SystemEventMessageBody : OutboundOUCHMessage {
        uint64_t timestamp;
        unsigned char event_code;
    } SystemEventMessage;

    //"A"
    typedef struct __attribute__((__packed__))AcceptedMessageBody : OutboundOUCHMessage {
        uint64_t timestamp;
        unsigned char order_token[14];
        unsigned char buy_sell_indicator;
        uint32_t shares;
        unsigned char stock[8];
        uint32_t price;
        uint32_t time_in_force;
        unsigned char firm[4];
        unsigned char display;
        uint64_t reference_number;
        unsigned char capacity;
        unsigned char intermarket_sweep_eligibility;
        unsigned char minimum_quantity[4];
        unsigned char cross_type;
        unsigned char order_state;
        unsigned char bbo_weight_indicator;
    } AcceptedMessage;

    //"U"
    typedef struct __attribute__((__packed__))ReplacedMessageBody : OutboundOUCHMessage {
        uint64_t timestamp;
        unsigned char replacement_order_token[14];
        unsigned char buy_sell_indicator;
        uint32_t shares;
        unsigned char stock[8];
        uint32_t price;
        uint32_t time_in_force;
        unsigned char firm[4];
        unsigned char display;
        uint64_t reference_number;
        unsigned char capacity;
        unsigned char intermarket_sweep_eligibility;
        uint32_t minimum_quantity;
        unsigned char cross_type;
        unsigned char order_state;
        unsigned char previous_order_token[14];
        unsigned char bbo_weight_indicator;
    } ReplacedMessage;

    //"C"
    typedef struct __attribute__((__packed__))CanceledMessageBody : OutboundOUCHMessage {
        uint64_t timestamp;
        unsigned char mpid[4];
        unsigned char order_token[14];
        uint32_t decrement_shares; // This number is incremental, not cumulative
        unsigned char reason;
    } CanceledMessage;

    //"D"
    typedef struct __attribute__((__packed__))AIQCanceledMessageBody : OutboundOUCHMessage {
        uint64_t timestamp;
        unsigned char order_token[14];
        uint32_t decrement_shares;
        unsigned char reason;
        uint32_t quantity_prevented_from_trading;
        uint32_t execution_price;
        unsigned char liquidity_flag;
    } AIQCanceledMessage;

    //"E"
    typedef struct __attribute__((__packed__))ExecutedMessageBody : OutboundOUCHMessage {
        uint64_t timestamp;
        char mpid[4];
        char order_token[14];
        uint32_t executed_shares;
        uint32_t executed_price;
        char liquidity_flag; // NOT USED
        uint64_t match_number; // Unique for every match, same on buy/sell
    } ExecutedMessage;

    //"B"
    typedef struct __attribute__((__packed__))BrokenTradeMessageBody : OutboundOUCHMessage {
        uint64_t timestamp;
        unsigned char order_token[14];
        uint64_t match_number;
        unsigned char reason;
    } BrokenTradeMessage;

    //"G"
    typedef struct __attribute__((__packed__))ExecutedwithReferencePriceMessageBody : OutboundOUCHMessage {
        uint64_t timestamp;
        unsigned char order_token[14];
        uint32_t executed_shares;
        uint32_t execution_price;
        unsigned char liquidity_flag;
        uint64_t match_number;
        uint32_t reference_price;
        unsigned char reference_price_type;
    } ExecutedwithReferencePriceMessage;

    //"F"
    typedef struct __attribute__((__packed__))TradeCorrectionMessageBody : OutboundOUCHMessage {
        uint64_t timestamp;
        unsigned char order_token[14];
        uint32_t executed_shares;
        uint32_t execution_price;
        unsigned char liquidity_flag;
        uint64_t match_number;
        unsigned char reason;
    } TradeCorrectionMessage;

    //"J"
    typedef struct __attribute__((__packed__))RejectedMessageBody : OutboundOUCHMessage {
        uint64_t timestamp;
        unsigned char order_token[14];
        unsigned char reason;
    } RejectedMessage;

    //"P"
    typedef struct __attribute__((__packed__))CancelPendingMessageBody : OutboundOUCHMessage {
        uint64_t timestamp;
        unsigned char order_token[14];
    } CancelPendingMessage;

    //"I"
    typedef struct __attribute__((__packed__))CancelRejectMessageBody : OutboundOUCHMessage {
        uint64_t timestamp;
        unsigned char mpid[4];
        unsigned char order_token[14];
    } CancelRejectMessage;

    //"T"
    typedef struct __attribute__((__packed__))OrderPriorityUpdateMessageBody : OutboundOUCHMessage {
        uint64_t timestamp;
        unsigned char order_token[14];
        unsigned char price;
        unsigned char display;
        uint64_t order_reference_number;
    } OrderPriorityUpdateMessage;

    //"M"
    typedef struct __attribute__((__packed__))OrderModifiedMessageBody : OutboundOUCHMessage {
        uint64_t timestamp;
        unsigned char order_token[14];
        unsigned char buy_sell_indicator;
        uint32_t shares;
    } OrderModifiedMessage;
    //"N"
    typedef struct __attribute__((__packed__))TradeNowMessageBody : OutboundOUCHMessage {
        uint64_t timestamp;
        unsigned char order_token[14];
    } TradeNowMessage;
}