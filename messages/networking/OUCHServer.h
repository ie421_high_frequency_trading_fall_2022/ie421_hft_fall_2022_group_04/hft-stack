#pragma once

#include <stdexcept>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

class OUCHServer {
private:
    int sock;
    int client_sock;
    struct sockaddr_in addr;

public:
    OUCHServer(int port);

    void sendMessage(void *message, size_t length);

    void close_client();

    void close_sock();
};
