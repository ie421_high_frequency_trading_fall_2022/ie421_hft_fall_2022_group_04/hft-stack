#include "OUCHServer.h"

OUCHServer::OUCHServer(int port) {
    // Create socket
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0) {
        throw std::runtime_error("Failed to create socket");
    }

    // Set socket options
    int opt = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) < 0) {
        throw std::runtime_error("Failed to set socket options");
    }

    // Bind socket
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons(port);
    if (bind(sock, (struct sockaddr*) &addr, sizeof(addr)) < 0) {
        throw std::runtime_error("Failed to bind socket");
    }

    // Listen for incoming connections
    listen(sock, SOMAXCONN);

    socklen_t addr_len = sizeof(addr);

    // Accept incoming connection
    client_sock = accept(sock, (struct sockaddr*) &addr, (socklen_t*)&addr_len);
    if (client_sock < 0) {
        throw std::runtime_error("Failed to accept incoming connection");
    }
}

void OUCHServer::sendMessage(void *message, size_t length) {
    // Send message
    send(client_sock, message, length, 0);
}

void OUCHServer::close_client() {
    close(client_sock);
}

void OUCHServer::close_sock() {
    close(sock);
}
