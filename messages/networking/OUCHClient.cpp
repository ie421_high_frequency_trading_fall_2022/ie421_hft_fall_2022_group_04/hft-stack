#include "OUCHClient.h"

using namespace std;

void parse_ouch(int socket, std::queue<OutboundOUCH::OutboundOUCHMessage*> &q);


OUCHClient::OUCHClient(std::string server_ip, int server_port) : server_ip(server_ip), server_port(server_port)
{
    // Create socket
    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0) {
        throw std::runtime_error("Failed to create socket");
    }

    // Connect to server
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(server_port);
    if (inet_pton(AF_INET, server_ip.c_str(), &server_addr.sin_addr) <= 0) {
        throw std::runtime_error("Invalid IP address");
    }
    while (connect(sock, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
        std::cout << "Failed to connect to server. Retrying..." << std::endl;
        sleep(1);
    }
    // if (connect(sock, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0) {
    //     throw std::runtime_error("Failed to connect to server");
    // }
}

void OUCHClient::sendMessage(void *msg ,int length) 
{
    send(sock, msg, length, 0);
}

void OUCHClient::receiveMessages(std::queue<OutboundOUCH::OutboundOUCHMessage*> &q)
{
    while(true)
    {
        parse_ouch(sock, q);
    }
}


void parse_ouch(int socket, std::queue<OutboundOUCH::OutboundOUCHMessage*> &q)
{
    char type;
    size_t bytesRead;
    bytesRead = recv(socket, &type, sizeof(type), 0);
    if (bytesRead == 0) {
        return;
    }
    // std::cout << "OUCH message received: " << std::endl;
    // printf("%x ", type);
    // printf("%d ", (uint)type);
    // printf("%x\n", (unsigned char)type);
    switch (type) {
        case 'S': {
            OutboundOUCH::SystemEventMessage *message = new OutboundOUCH::SystemEventMessage();
            message->type = type;
            bytesRead = recv(socket,(char*)message + TYPE_OFFSET, sizeof(OutboundOUCH::SystemEventMessage) - TYPE_OFFSET, 0);
            if (bytesRead == 0) {
                break;
            }
            std::cout << "System event message received: " << message->event_code << std::endl;
            // std::cout << message->timestamp << std::endl;
            q.push(message);
            break;
        }
        case 'A': {
            OutboundOUCH::AcceptedMessage *message = new OutboundOUCH::AcceptedMessage();
            message->type = type;
            bytesRead = recv(socket, (char*)message + TYPE_OFFSET, sizeof(OutboundOUCH::AcceptedMessage) - TYPE_OFFSET, 0);
            if (bytesRead == 0) {
                break;
            }
            std::cout << "Accepted message received: " << message->order_token << std::endl;
            // std::cout << message->buy_sell_indicator << endl;
            // std::cout << message->shares << endl;
            q.push(message);
            // print other fields as appropriate
            break;
        }
        case 'U': {
            OutboundOUCH::ReplacedMessage *message = new OutboundOUCH::ReplacedMessage();
            message->type = type;
            bytesRead = recv(socket, (char*)message + TYPE_OFFSET, sizeof(OutboundOUCH::ReplacedMessage) - TYPE_OFFSET, 0);
            if (bytesRead == 0) {
                break;
            }
            std::cout << "Replaced message received: " << message->replacement_order_token << std::endl;
            // std::cout << message->shares << endl;
            // std::cout << message->stock << endl;
      
           
            q.push(message);
            // print other fields as appropriate
            break;
        }

        case 'C': {
            OutboundOUCH::CanceledMessage *message = new OutboundOUCH::CanceledMessage();
            message->type = type;
            bytesRead = recv(socket, (char*)message + TYPE_OFFSET, sizeof(OutboundOUCH::CanceledMessage) - TYPE_OFFSET, 0);
            if (bytesRead == 0) {
                break;
            }
            std::cout << "Cancelled message received: " << message->order_token << std::endl;
            // std::cout << message->decrement_shares << endl;
            // std::cout << message->reason << endl;
            q.push(message);
            // print other fields as appropriate
            break;
        }

        case 'D': {
            OutboundOUCH::AIQCanceledMessage *message = new OutboundOUCH::AIQCanceledMessage();
            message->type = type;
            bytesRead = recv(socket, (char*)message + TYPE_OFFSET, sizeof(OutboundOUCH::AIQCanceledMessage) - TYPE_OFFSET, 0);
            if (bytesRead == 0) {
                break;
            }
            std::cout << "AIQ Cancelled message received: " << message->order_token << std::endl;
            // std::cout << message->decrement_shares << endl;
            // std::cout << message->reason << endl;
            // std::cout << message->quantity_prevented_from_trading << endl;
            // std::cout << message->execution_price << endl;
            // std::cout << message->liquidity_flag << endl;
            q.push(message);
            // print other fields as appropriate
            break;
        }
        case 'E': {
            OutboundOUCH::ExecutedMessage *message = new OutboundOUCH::ExecutedMessage();
            message->type = type;
            bytesRead = recv(socket, (char*)message + TYPE_OFFSET, sizeof(OutboundOUCH::ExecutedMessage) - TYPE_OFFSET, 0);
            if (bytesRead == 0) {
                break;
            }
            // std::cout << "Executed message received: " << message->order_token << std::endl;
            // std::cout << "Executed shares:" << message->executed_shares << endl;
            // std::cout << "Execution price:" << message->executed_price << endl;
            // std::cout << "Liquidity flag:" << message->liquidity_flag << endl;
            // std::cout << "Match number:" << message->match_number << endl;
            q.push(message);
            // print other fields as appropriate
            break;
        }

        case 'B': {
            OutboundOUCH::BrokenTradeMessage *message = new OutboundOUCH::BrokenTradeMessage();
            message->type = type;
            bytesRead = recv(socket, (char*)message + TYPE_OFFSET, sizeof(OutboundOUCH::BrokenTradeMessage) - TYPE_OFFSET, 0);
            if (bytesRead == 0) {
                break;
            }
            std::cout << "Broken trade message received: " << message->order_token << std::endl;
            // std::cout << message->match_number << endl;
            // std::cout << message->reason << endl;
            q.push(message);
            // print other fields as appropriate
            break;
        }

        case 'G': {
            OutboundOUCH::ExecutedwithReferencePriceMessage *message = new OutboundOUCH::ExecutedwithReferencePriceMessage();
            message->type = type;
            bytesRead = recv(socket, (char*)message + TYPE_OFFSET, sizeof(OutboundOUCH::ExecutedwithReferencePriceMessage) - TYPE_OFFSET,0);
            if (bytesRead == 0) {
                break;
            }
            std::cout << "Executed with reference price message received: " << message->order_token << std::endl;
            // std::cout << message->executed_shares << endl;
            // std::cout << message->execution_price << endl;
            // std::cout << message->liquidity_flag << endl;
            // std::cout << message->match_number << endl;
            // std::cout << message->reference_price << endl;
            // std::cout << message->reference_price_type << endl;
            q.push(message);
            // print other fields as appropriate
            break;
        }
    }
}