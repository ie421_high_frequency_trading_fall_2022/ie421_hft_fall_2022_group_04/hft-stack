#include <iostream>
#include <stdexcept>
#include <queue>
#include <string>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "nasdaq_itch.h"


void process_itch_message(unsigned char *buff);

class ITCHClient {
    private:
        int sock;
        struct ip_mreq mreq;
        struct sockaddr_in addr;

    public:
        ITCHClient(std::string multicast_ip, short multicast_port);
        void listen(std::queue<ITCH::ITCHMessage*> &q);
};

