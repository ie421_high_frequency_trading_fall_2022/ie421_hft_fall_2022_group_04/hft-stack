#include <iostream>
#include <stdexcept>
#include <queue>
#include <string>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include "nasdaq_ouch_outbound.h"

#define TYPE_OFFSET 1

class OUCHClient {
private:
    int sock;
    struct sockaddr_in server_addr;
    std::string server_ip;
    int server_port;

public:
    OUCHClient(std::string server_ip, int server_port);

    // Send a single OUCH message to the server
    void sendMessage(void *msg ,int length);

    void receiveMessages(std::queue<OutboundOUCH::OutboundOUCHMessage*> &q);
};

