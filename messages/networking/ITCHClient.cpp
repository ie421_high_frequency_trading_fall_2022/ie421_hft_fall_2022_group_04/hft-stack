 #include "ITCHClient.h"
 #include <cstring>

void process_itch_message(unsigned char *buff, std::queue<ITCH::ITCHMessage*> &q);
uint64_t convertToInteger(unsigned char* arr);

ITCHClient::ITCHClient(std::string multicast_ip, short multicast_port)
{
    // Create socket
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0) {
        throw std::runtime_error("Failed to create socket");
    }

    // Join multicast group
    mreq.imr_multiaddr.s_addr = inet_addr(multicast_ip.c_str());
    mreq.imr_interface.s_addr = INADDR_ANY;
    if (setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq)) < 0) {
        throw std::runtime_error("Failed to join multicast group");
    }

    // Bind socket
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons(multicast_port);
    if (bind(sock, (struct sockaddr*) &addr, sizeof(addr)) < 0) {
        throw std::runtime_error("Failed to bind socket");
    }
}

void ITCHClient::listen(std::queue<ITCH::ITCHMessage*> &q)
{
    socklen_t addr_len = sizeof(addr);

    std::cout << "Waiting for messages" << std::endl;
    while (true) {
        unsigned char *buff = new unsigned char[128];
        socklen_t addr_len = sizeof(addr);
        int bytesRead = recvfrom(sock, buff, 128, 0, (struct sockaddr *) &addr, (socklen_t*)&addr_len);
        if (bytesRead == 0) {
            delete[] buff;
            break;
        }
        process_itch_message(buff, q) ;
    }
}




void process_itch_message(unsigned char *buff, std::queue<ITCH::ITCHMessage*> &q) {
    std::cout << "ITCH Received message: " << buff[0]  << std::endl;
    char type = buff[0];
        switch (type)
        {
            case 'S': 
            {
                ITCH::SystemEventMessage* systemEvent = reinterpret_cast<ITCH::SystemEventMessage*>(buff);
                std::cout << "Received System Event Message" << std::endl;
                std::cout << "stock locate: " << systemEvent->stock_locate << std::endl;
                std::cout << "stock tracking: " << systemEvent->stock_tracking << std::endl;
                std::cout << "timestamp: " << convertToInteger(systemEvent->timestamp) << std::endl;
                std::cout << "event code: " << systemEvent->event_code << std::endl;
                q.push(systemEvent);
                break;
            }
            case 'F':
            {
                ITCH::AddOrderMessageWithMPID* addOrder = reinterpret_cast<ITCH::AddOrderMessageWithMPID*>(buff);
                std::cout << "Received Add Order Message With MPID" << std::endl;
                std::cout << "stock locate: " << addOrder->stock_locate << std::endl;
                std::cout << "stock tracking: " << addOrder->stock_tracking << std::endl;
                std::cout << "timestamp: " << convertToInteger(addOrder->timestamp) << std::endl;
                std::cout << "order reference number: " << addOrder->order_reference_number << std::endl;
                std::cout << "buy sell indicator: " << addOrder->buy_sell_indicator << std::endl;
                std::cout << "shares: " << addOrder->shares << std::endl;
                std::cout << "stock: " << addOrder->stock << std::endl;
                std::cout << "price: " << addOrder->price << std::endl;
                std::cout << "attribution: " << addOrder->attribution << std::endl;
                q.push(addOrder);
                break;
            }
            case 'E' :
            {
                ITCH::OrderExecutedMessage* orderExecuted = reinterpret_cast<ITCH::OrderExecutedMessage*>(buff);
                std::cout << "Received Order Executed Message" << std::endl;
                std::cout << "stock locate: " << orderExecuted->stock_locate << std::endl;
                std::cout << "stock tracking: " << orderExecuted->stock_tracking << std::endl;
                std::cout << "timestamp: " << convertToInteger(orderExecuted->timestamp) << std::endl;
                std::cout << "order reference number: " << orderExecuted->order_reference_number << std::endl;
                std::cout << "executed shares: " << orderExecuted->executed_shares << std::endl;
                std::cout << "match number: " << orderExecuted->match_number << std::endl;
                q.push(orderExecuted);
                break;
            }
            case 'C' :
            {
                ITCH::OrderExecutedWithPriceMessage* orderExecutedWithPrice = reinterpret_cast<ITCH::OrderExecutedWithPriceMessage*>(buff);
                std::cout << "Received Order Executed With Price Message" << std::endl;
                std::cout << "stock locate: " << orderExecutedWithPrice->stock_locate << std::endl;
                std::cout << "stock tracking: " << orderExecutedWithPrice->stock_tracking << std::endl;
                std::cout << "timestamp: " << convertToInteger(orderExecutedWithPrice->timestamp) << std::endl;
                std::cout << "order reference number: " << orderExecutedWithPrice->order_reference_number << std::endl;
                std::cout << "executed shares: " << orderExecutedWithPrice->executed_shares << std::endl;
                std::cout << "match number: " << orderExecutedWithPrice->match_number << std::endl;
                std::cout << "printable: " << orderExecutedWithPrice->printable << std::endl;
                std::cout << "execution price: " << orderExecutedWithPrice->execution_price << std::endl;
                q.push(orderExecutedWithPrice);
                break;
            }
            case 'X' :
            {
                ITCH::OrderCancelMessage* orderCancel = reinterpret_cast<ITCH::OrderCancelMessage*>(buff);
                std::cout << "Received Order Cancel Message" << std::endl;
                std::cout << "stock locate: " << orderCancel->stock_locate << std::endl;
                std::cout << "stock tracking: " << orderCancel->stock_tracking << std::endl;
                std::cout << "timestamp: " << convertToInteger(orderCancel->timestamp) << std::endl;
                std::cout << "order reference number: " << orderCancel->order_reference_number << std::endl;
                std::cout << "cancelled shares: " << orderCancel->cancelled_shares << std::endl;
                q.push(orderCancel);
                break;
            }
            case 'D' :
            {
                ITCH::OrderDeleteMessage* orderDelete = reinterpret_cast<ITCH::OrderDeleteMessage*>(buff);
                std::cout << "Received Order Delete Message" << std::endl;
                std::cout << "stock locate: " << orderDelete->stock_locate << std::endl;
                std::cout << "stock tracking: " << orderDelete->stock_tracking << std::endl;
                std::cout << "timestamp: " << convertToInteger(orderDelete->timestamp) << std::endl;
                std::cout << "order reference number: " << orderDelete->order_reference_number << std::endl;
                q.push(orderDelete);
                break;
            }
            case 'U' :
            {
                ITCH::OrderReplaceMessage* orderReplace = reinterpret_cast<ITCH::OrderReplaceMessage*>(buff);
                std::cout << "Received Order Replace Message" << std::endl;
                std::cout << "stock locate: " << orderReplace->stock_locate << std::endl;
                std::cout << "stock tracking: " << orderReplace->stock_tracking << std::endl;
                std::cout << "timestamp: " << convertToInteger(orderReplace->timestamp) << std::endl;
                std::cout << "original order reference number: " << orderReplace->order_reference_number << std::endl;
                std::cout << "new order reference number: " << orderReplace->new_order_reference_number << std::endl;
                std::cout << "shares: " << orderReplace->shares << std::endl;
                std::cout << "price: " << orderReplace->price << std::endl;
                q.push(orderReplace);
                break;
            }
            default: 
            {
                exit(0);
            }
    }
}

uint64_t convertToInteger(unsigned char* arr)
{
    uint64_t x;          // the integer to store the result
    memcpy(&x, arr, 6);  // copy the lower 48 bits from the array into x
    x &= 0x0000FFFFFFFFFFFF;

    return x;
}

