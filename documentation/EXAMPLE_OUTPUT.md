# Example and Expected Outputs

From the order matching engine:

![image-3.png](./image-3.png)

From the trading stack:

![image-4.png](./image-4.png)
