# Exchange Components

## Exchange Structs

The internal format uses a struct that retains the same NASDAQ OUCH and ITCH specification. However, note that timestamping for NASDAQ ITCH is specified as 6 bytes but internally we use 8 bytes. Unless otherwise noted, structs use char arrays and any messages that are parsed into a buffer array to be set out to the network use an unsigned char*. This latter decision is for readability purposes. We will enumerate below all the associated types in our Exchange that we have structs for. The ones with an asterisk (*) at the end are not currently being used.

For more information on the NASDAQ ITCH/OUCH specification, please refer to these two documents from NASDAQ.

- [NASDAQ OUCH v4.2](http://www.nasdaqtrader.com/content/technicalsupport/specifications/tradingproducts/ouch4.2.pdf)
- [NASDAQ ITCH v5.0](https://www.nasdaqtrader.com/content/technicalsupport/specifications/dataproducts/NQTVITCHSpecification.pdf)

### NASDAQ OUCH Inbound (exchange/src/structs/nasdaq_ouch.h) - All orders inherit from `InboundOrder`

```cpp
virtual InboundOrder;
OrderEntry;
OrderReplace;*
OrderCancel;
OrderModify;*
```

### NASDAQ OUCH Outbound (exchange/src/structs/nasdaq_ouch.h) - All orders inherit from `OutboundOrder`

```cpp
virtual OutboundOrder;
SystemEventMessage;*
OrderAcceptedOutbound;
OrderReplacedOutbound;*
OrderCancelledOutbound;
OrderExecutedOutbound;
RejectedMessageOutbound;*
CancelRejectOutbound;
OrderPriorityUpdateOutbound;*
```

### NASDAQ ITCH Outbound (exchange/src/structs/nasdaq_itch.h) - All orders inherit from `ITCH_Message`

```cpp
virtual ITCH_Message;
ITCH_AddOrderMPID;
ITCH_OrderExecutedPrice;
ITCH_OrderCancel;
```

## Order Matching Engine (OME)

The OME is in charge of the following:

- Converting from byte array to struct
- Adding order to orderbook
- Repacking the struct output for OUCH/ITCH into byte array
- Sending packet to Gateways

### **Event Loop**

To visualize this process and gain an understanding of how the OME works, it is best to gain an understanding of the event loop by viewing it through the lifetime of an order, in this case, an `OrderEntry`.

```cpp
int main() {

  // Async read in inputs from TCP connections into queue storing OUCH inbound orders
  add_inbound_packet_to_queue(OUCH_inbound_packet_queue);

  // Convert OUCH packets to orders
  convert_OUCH_inbound_packet_to_order(OUCH_inbound_packet_queue, OUCH_inbound_order_queue);

  // Add OUCH inbound order to the corresponding OrderBook based on the stock symbol and 
  // add queue orderbook to process next order.
  add_inbound_order_to_orderbook(OUCH_inbound_order_queue, orderbook_symbol_to_execute_queue);

  // Process orders in the inbound order queues for OrderBooks that have inbound orders
  // Add the symbol to the OUCH and ITCH output queues to make sure we send out any outputs from
  // the OrderBook back to the Gateway/Tickerplant
  execute_orderbook_next(orderbook_symbol_to_execute_queue, orderbook_symbol_OUCH_to_output_queue, orderbook_symbol_ITCH_to_output_queue);

  // Grabs the OUCH outbound from OrderBook. The symbol is given through the orderbook_symbol_OUCH_to_output_queue.
  grab_OUCH_outbound_from_orderbook(orderbook_symbol_OUCH_to_output_queue, outbound_order_queue);

  // Grabs the ITCH outbound from OrderBook. The symbol is given through the orderbook_symbol_ITCH_to_output_queue.
  grab_ITCH_outbound_from_orderbook(orderbook_symbol_ITCH_to_output_queue, ITCH_outbound_order_queue);

  // Converts OUCH outbound orders to an unsigned char buffer to be later sent out.
  convert_OUCH_outbound_order_to_packet(outbound_order_queue, outbound_packet_queue);

  // Converts outbound ITCH orders to an unsigned char buffer to be later sent out.
  convert_ITCH_outbound_order_to_packet(ITCH_outbound_order_queue, outbound)

  // Send the outbound packet back to the Gateway to be sent to the market participant.
  send_outbound_OUCH_packet(outbound_packet_queue);

  // Send the ITCH outbound packet to the Tickerplant to be sent to the market data feed.
  send_outbound_ITCH_packet(ITCH_outbound_packet_queue);
}
```

### **Example: Order Entry Buy 1000 AAPL @ $100**

Now we will follow an Order Entry message through our system to better understand how it gets processed. We are given the following Order Entry message:

```cpp
OrderEntry order;
order.type = 'O';
order.token = "0001";
order.buy_sell_indicator = 'B';
order.shares = 1000;
order.stock_sym = "AAPL";
order.price = 100;
order.mpid = "Behman";
```

This message is converted into a byte array following NASDAQ OUCH specification. To see an implementation, check out `OUCH_Parser::_order_entry` method either on the Doxygen or in the source code under `src/parser/parser.h`. We start by sending the byte array through TCP to an open socket on the OME. The following is the lifetime cycle of this order.

1. `add_inbound_packet_to_queue` ***(TBD)***: Message is received at the OME and read into a `unsigned` 128 byte `char` buffer that is then added to the OME's `OUCH_inbound_buffer_queue_`.
2. `convert_OUCH_inbound_packet_to_order`: Packet is read off the `OUCH_inbound_buffer_queue_`, converted into a struct with `OUCH_Parser::parse_inbound`, and stored in `OUCH_inbound_order_queue_` as an `InboundOrderUniquePtr`.
3. `add_inbound_order_to_orderbook`: The order is read off the `OUCH_inbound_order_queue_` and depending on the `order.stock_sym`, is sent to the correct `OrderBook`, in this case `OrderBook("AAPL")`'s `inbound_orders_`.
4. `execute_orderbook_next`: The thread then calls `orderbook.process_next_order()` which will read off the orderbook's `inbound_orders_` and then process it accordintly. In this case, assuming no prior existing orders at that price level or below, the order will rest on the orderbook. One OUCH message and one ITCH message are generated. The OUCH message is an `OrderAcceptedOutbound` message and the ITCH message is an `ITCH_AddOrderMPID`, which represents an add order entry with an mpid attribution. These are both sent to the orderbook's `outbound_orders_` queue and `outbound_ITCH_messages_` queue respectively. Thus, we should see a size of one on each of these queues. Furthermore, by the end of this process, the OME will add the `std::string` corresponding to `order.stock_sym` into the OME's `orderbook_symbol_OUCH_to_output_queue_` and `orderbook_symbol_ITCH_to_output_queue_` to then process the outbound queues of the orderbook corresponding to that stock symbol.
5. `grab_OUCH_outbound_from_orderbook`: We then grab ***ALL*** messages currently residing in that orderbook's `outbound_orders_` queue. In our example, it should only contain the outbound OUCH order of type `OrderAcceptedOutbound`. This OUCH `OutboundOrder` is then put in the OME's `OUCH_outbound_order_queue_`.
6. `grab_ITCH_outbound_from_orderbook`: We grab ***ALL*** messages currently residing in the orderbooks `outbound_ITCH_messages_` queue. In our example, it should only contain the outbound ITCH order of type `ITCH_AddOrderMPID`. This ITCH struct is then put in the OME's `ITCH_outbound_order_queue_`.
7. `convert_OUCH_outbound_order_to_packet`: We then take in the OUCH orders from the `OUCH_outbound_order_queue_` and convert them into `unsigned char*` of size 128 bytes packets to be later sent out. The message is then added to the `OUCH_outbound_buffer_queue_`. At the end of our example, there should be size 0 in the first queue and size 1 in the latter queue.
8. `convert_ITCH_outbound_order_to_packet`: We do the same with any ITCH messages popping from the `ITCH_outbound_order_queue_` and adding the packets into the `ITCH_outbound_buffer_queue_`. At the end of our example, there should be size 0 in the first queue and size 1 in the latter queue.
9. `send_outbound_OUCH_packet` ***(TBD)***: We then send the packets by popping from the `OUCH_outbound_order_queue_` and sending them to the appropriate gateway(s). In our example, the `OUCH_outbound_order_queue_` will have a size of 0 after this operation.
10. `send_outbound_ITCH_packet` ***(TBD)***: We also send the ITCH packets by popping from the `ITCH_outbound_buffer_queue_` and sending them to the Tickerplant(s). In our example, when the operation finishes the `ITCH_outbound_buffer_queue_` will have size 0.

### OME Code Base Overview (exchange/)

Now that you understand how an order goes through the OME, we will expand upon the code base at a high level to gain an understanding of how it all fits together. A deeper dive can be done by looking through the code documentation through doxygen. Here we will expand upon the most important classes and give a high level overview of what functionality they cover. Note that the list follows the outline provided by the file structure of the OME repository.

> Note: This overview is **not** an overview of the file system, but an overview of the classes. For an overview of the filesystem, please see [GIT_REPO_PROJECT_LAYOUT](./GIT_REPO_PROJECT_LAYOUT.md).

> Note: Classes that shared pointers have the form `std::shared_ptr<Classes> ==> ClassesPtr` and unique pointers have the form `std::unique_ptr<Classes> ==> ClassesUniquePtr`. These are usually done as a typedef following the declaration of the class or struct.

#### **Networking** (*src/networking/*)

- `Session` (./session.h): Maintains a `boost::asio::ip::tcp::socket` and any information needed for reading and writing, including two queues for inbound and outbound messages.
- `SessionManager` (./session_manager.h): Contains a mapping from the `std::string(ipv4:port)` to the `SessionPtr` (aka. `std::shared_ptr<Session>`). This is used later on to for-loop through to send apprioriate messages and receive messages.

#### **Order Matching Engine** (*src/order_matching_engine/*)

- `Timer/MachineTimer/Simpletimer` (*./machine_time.h*): These classes have code that help determine the machine time for timestamping orders. In the future, probably should use some external method like PPS or something similar to set the time.
- `OrderMatchingEngine` (*./order_matching_engine.h*): This is the OME class that runs the event loop mentioned [above](#event-loop).

#### **Order Book** (*src/orderbook/*)

- `OrderBook` (*./orderbook.h*): Contains the orderbook logic including adding the orders into the queue and processing them by updating the orderbook.

#### **Parser** (*src/parser/*)

- `Parser/OUCH_Parser/ITCH_Parser` (*./parser.h*): Static classes that help parse OUCH and ITCH orders. If they functions are converting packets of type `unsigned char*` to an order struct they will be of the form `parse_inbound(unsigned char*)`, where inbound is relative to the exchange. If they functions are converting order structs to packets of type `unsigned char*` they will be of the form `parse_outbound(OrderPtr order, unsigned char[])`.

#### **Struct** (*src/struct/*)

- `InboundOrder/OutboundOrder` (*./nasdaq_ouch.h*): Structs for NADSAQ OUCH as explained in [Exchange Structs](#exchange-structs)
- `ITCH_Message` (*./nasdaq_itch.h*): Structs for NASDAQ ITCH as explained in [Exchange Structs](#exchange-structs)

## OrderBook

// TODO

## Networking IO w/ Boost Asio

// TODO

## IP Addressing

Overall VM: 192.168.128.1

OME 1-255: 192.168.65.X
Gateway 1-255: 192.168.64.X
TP 1-255: 192.168.63.X
