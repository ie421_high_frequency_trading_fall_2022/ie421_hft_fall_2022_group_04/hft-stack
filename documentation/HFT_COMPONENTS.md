# HFT Stack components

The internal format uses a struct that retains the same NASDAQ OUCH and ITCH specification. These structures are all packed to make it easier to interface with the network. 

For more information on the NASDAQ ITCH/OUCH specification, please refer to these two documents from NASDAQ.

- [NASDAQ OUCH v4.2](http://www.nasdaqtrader.com/content/technicalsupport/specifications/tradingproducts/ouch4.2.pdf)
- [NASDAQ ITCH v5.0](https://www.nasdaqtrader.com/content/technicalsupport/specifications/dataproducts/NQTVITCHSpecification.pdf)

## Table of Contents

- [ITCH Namespace](#namespace-itch)
  - [ITCHMessage](#structure-itchmessage)
  - [SystemEventMessage](#structure-systemeventmessage)
  - [AddOrderMessageWithMPID](#structure-addordermessagewithmpid)
  - [OrderExecutedMessage](#structure-orderexecutedmessage)
  - [OrderExecutedWithPriceMessage](#structure-orderexecutedwithpricemessage)
  - [OrderCancelMessage](#structure-ordercancelmessage)
  - [OrderDeleteMessage](#structure-orderdeletemessage)
  - [OrderReplaceMessage](#structure-orderreplacemessage)
- [InboundOUCH Namespace](#namespace-inboundouch)
  - [InboundOUCHMessage](#structure-inboundouchmessage)
  - [EnterOrderMessage](#structure-enterordermessage)
  - [ReplaceOrderMessage](#structure-replaceordermessage)
  - [CancelOrderMessage](#structure-cancelordermessage)
- [OutboundOUCH Namespace](#namespace-outboundouch)
  - [OutboundOUCHMessage](#structure-outboundouchmessage)
  - [SystemEventMessage](#structure-systemeventmessage-1)
  - [AcceptedMessage](#structure-acceptedmessage)
  - [RejectedMessage](#structure-rejectedmessage)
  - [CancelPendingMessage](#structure-cancelpendingmessage)
  - [CancelRejectedMessage](#structure-cancelrejectedmessage)
  - [OrderExecutedMessage](#structure-orderexecutedmessage-1)
  - [OrderReplacedMessage](#structure-orderreplacedmessage)
- [Order Book](#order-book)
  - [Overview](#overview)
  - [Order Book Functions](#order-book-functions)
  - [Message Type Interaction](#message-type-interaction)
- [Backtesting](#backtesting)

## Namespace: ITCH

### Structure: ITCHMessage
- type (unsigned char)

### Structure: SystemEventMessage
- Inherits: ITCHMessage
- stock_locate (uint16_t)
- stock_tracking (uint16_t)
- timestamp (unsigned char[6])
- event_code (unsigned char)

### Structure: AddOrderMessageWithMPID
- Inherits: ITCHMessage
- stock_locate (uint16_t)
- stock_tracking (uint16_t)
- timestamp (unsigned char[6])
- order_reference_number (uint64_t)
- buy_sell_indicator (unsigned char)
- shares (uint32_t)
- stock (uint64_t)
- price (uint32_t)
- attribution (uint32_t)

### Structure: OrderExecutedMessage
- Inherits: ITCHMessage
- stock_locate (uint16_t)
- stock_tracking (uint16_t)
- timestamp (unsigned char[6])
- order_reference_number (uint64_t)
- executed_shares (uint32_t)
- match_number (uint64_t)

### Structure: OrderExecutedWithPriceMessage
- Inherits: ITCHMessage
- stock_locate (uint16_t)
- stock_tracking (uint16_t)
- timestamp (unsigned char[6])
- order_reference_number (uint64_t)
- executed_shares (uint32_t)
- match_number (uint64_t)
- printable (unsigned char)
- execution_price (uint32_t)

### Structure: OrderCancelMessage
- Inherits: ITCHMessage
- stock_locate (uint16_t)
- stock_tracking (uint16_t)
- timestamp (unsigned char[6])
- order_reference_number (uint64_t)
- cancelled_shares (uint32_t)

### Structure: OrderDeleteMessage
- Inherits: ITCHMessage
- stock_locate (uint16_t)
- stock_tracking (uint16_t)
- timestamp (unsigned char[6])
- order_reference_number (uint64_t)

### Structure: OrderReplaceMessage
- Inherits: ITCHMessage
- stock_locate (uint16_t)
- stock_tracking (uint16_t)
- timestamp (unsigned char[6])
- order_reference_number (uint64_t)
- new_order_reference_number (uint64_t)
- shares (uint32_t)
- price (uint32_t)

## Namespace: InboundOUCH

### Structure: InboundOUCHMessage
- type (unsigned char)

### Structure: EnterOrderMessage
- Inherits: InboundOUCHMessage
- order_token (unsigned char[14])
- buy_sell_indicator (unsigned char)
- shares (uint32_t)
- stock (unsigned char[8])
- price (uint32_t)
- time_in_force (uint32_t)
- firm (unsigned char[4])
- display (unsigned char)
- capacity (unsigned char)
- intermarket_sweep_eligibility (unsigned char)
- minimum_quantity (uint32_t)
- cross_type (unsigned char)
- customer_type (unsigned char)

### Structure: ReplaceOrderMessage
- Inherits: InboundOUCHMessage
- existing_order_token (unsigned char[14])
- replacement_order_token (unsigned char[14])
- shares (uint32_t)
- price (uint32_t)
- time_in_force (uint32_t)
- display (unsigned char)
- intermarket_sweep_eligibility (unsigned char)
- minimum_quantity (uint32_t)

### Structure: CancelOrderMessage
- Inherits: InboundOUCHMessage
- mpid (unsigned char[4])
- order_token (unsigned char[14])
- shares (uint32_t)

## Namespace: OutboundOUCH

### Structure: OutboundOUCHMessage
- type (unsigned char)

### Structure: SystemEventMessage
- Inherits: OutboundOUCHMessage
- event_code (unsigned char)
- timestamp (unsigned char[6])

### Structure: AcceptedMessage
- Inherits: OutboundOUCHMessage
- order_token (unsigned char[14])
- timestamp (unsigned char[6])
- shares (uint32_t)
- stock (unsigned char[8])
- price (uint32_t)
- time_in_force (uint32_t)
- firm (unsigned char[4])
- display (unsigned char)
- order_reference_number (uint64_t)
- capacity (unsigned char)
- intermarket_sweep_eligibility (unsigned char)
- minimum_quantity (uint32_t)
- cross_type (unsigned char)
- customer_type (unsigned char)

### Structure: RejectedMessage
- Inherits: OutboundOUCHMessage
- order_token (unsigned char[14])
- timestamp (unsigned char[6])
- reason (unsigned char)

### Structure: CancelPendingMessage
- Inherits: OutboundOUCHMessage
- order_token (unsigned char[14])
- timestamp (unsigned char[6])

### Structure: CancelRejectedMessage
- Inherits: OutboundOUCHMessage
- order_token (unsigned char[14])
- timestamp (unsigned char[6])
- reason (unsigned char)

### Structure: OrderExecutedMessage
- Inherits: OutboundOUCHMessage
- order_token (unsigned char[14])
- timestamp (unsigned char[6])
- executed_shares (uint32_t)
- execution_price (uint32_t)
- liquidity_flag (unsigned char)
- match_number (uint64_t)

### Structure: OrderReplacedMessage
- Inherits: OutboundOUCHMessage
- replacement_order_token (unsigned char[14])
- timestamp (unsigned char[6])
- shares (uint32_t)
- price (uint32_t)
- time_in_force (uint32_t)
- display (unsigned char)
- order_reference_number (uint64_t)
- intermarket_sweep_eligibility (unsigned char)
- minimum_quantity (uint32_t)


## Order Book

### Overview

The `OrderBook` class is designed to manage and maintain buy and sell orders for a specific stock. Each order is represented by an `Order` struct, containing information such as order reference number, shares, price, and attribution. The buy-side and sell-side orders are stored in two separate maps, with the order reference number serving as the key.

### Order Book Functions

The `OrderBook` class provides several functions to handle different types of messages and update the order book accordingly:

1. `add_order()`: Adds a new order to the buy-side or sell-side depending on the buy/sell indicator.
2. `execute_order()`: Decreases the shares of an order by the executed shares or removes the order if executed shares equal the total shares.
3. `cancel_order()`: Decreases the shares of an order by the cancelled shares or removes the order if cancelled shares equal the total shares.
4. `delete_order()`: Removes an order from the buy-side or sell-side.
5. `replace_order()`: Updates an existing order with a new order reference number, shares, and price, then removes the old order.

### Message Type Interaction

The `OrderBook` class interacts with various ITCH message types to update its internal state:

- `AddOrderMessageWithMPID`: The `add_order()` function processes this message type to add a new order to the order book.
- `OrderExecutedMessage`: The `execute_order()` function processes this message type to update or remove an order based on executed shares.
- `OrderCancelMessage`: The `cancel_order()` function processes this message type to update or remove an order based on cancelled shares.
- `OrderDeleteMessage`: The `delete_order()` function processes this message type to remove an order from the order book.
- `OrderReplaceMessage`: The `replace_order()` function processes this message type to update an existing order with new information and remove the old order.

The `OrderBooks` class is a container for multiple `OrderBook` instances, with each instance representing a different stock. The class uses a map with stock_locate values as keys and `OrderBook` instances as values. The `OrderBooks` class provides the following functions for handling ITCH messages:

1. `add_order()`: Calls the `add_order()` function of the corresponding `OrderBook` instance based on the stock_locate value.
2. `execute_order()`: Calls the `execute_order()` function of the corresponding `OrderBook` instance based on the stock_locate value.
3. `cancel_order()`: Calls the `cancel_order()` function of the corresponding `OrderBook` instance based on the stock_locate value.
4. `delete_order()`: Calls the `delete_order()` function of the corresponding `OrderBook` instance based on the stock_locate value.
5. `replace_order()`: Calls the `replace_order()` function of the corresponding `OrderBook` instance based on the stock_locate value.

By handling different ITCH message types and interacting with the `OrderBook` instances, the `OrderBooks` class helps maintain an accurate representation of the order books for multiple stocks.


## Backtesting

The `Backtester` class is designed to run backtests on the given order books and inbound OUCH messages. It helps to evaluate the performance of a trading strategy by simulating its behavior over historical data. The class provides the following functions:

1. `Backtester(OrderBooks&, std::vector<InboundOUCH::EnterOrderMessage*>&)`: Constructor that initializes the `Backtester` with a reference to the order books and a vector of inbound OUCH enter order messages.
2. `run_backtest()`: Runs the backtest on the provided order books and inbound OUCH messages, calculating the total profit, total trades, and successful trades.
3. `process_trade(const InboundOUCH::EnterOrderMessage&)`: Processes an individual trade based on the provided enter order message, calculates the trade profit, and updates the corresponding order book.

The backtesting process starts by creating a `Backtester` instance with the required order books and inbound OUCH messages. The `run_backtest()` function is then called to initiate the backtest. For each inbound OUCH enter order message, the `process_trade()` function is called to simulate the trade and update the corresponding order book. The profits of each trade are accumulated, and the results are displayed at the end of the backtest.



