#include <thread>
#include <random>
#include <chrono>
#include <iostream>
#include <queue>
#include "nasdaq_ouch_outbound.h"
#include "nasdaq_ouch_inbound.h"
#include "nasdaq_itch.h"
#include "networking/ITCHClient.h"
#include "networking/OUCHClient.h"
#include "networking/OUCHServer.h"
#include "Backtester.h"
#include "algorithm.h"
#include "OrderBook.h"


#define TYPE_OFFSET 1


OrderBooks books;
std::queue<ITCH::ITCHMessage*> ITCHQueue;
std::queue<OutboundOUCH::OutboundOUCHMessage*> OutOUCHQueue;
std::vector<InboundOUCH::EnterOrderMessage*> InOUCHMessages;

void sendExampleInboundOUCH(OUCHServer& server);

void ITCHthreadFunction()
{
    ITCHClient client("239.255.0.1", 4003);
    client.listen(ITCHQueue);
    // printCorrectMessage
}

void OutboundOUCHthreadFunction()
{
    OUCHClient client("192.168.56.2", 3000);
    client.receiveMessages(OutOUCHQueue);
}

void MarketAnalysisThreadFunction(TradingStrategy& strategy) {
    OUCHServer server(3001);
    sendExampleInboundOUCH(server);
    while (true) {
        InboundOUCH::EnterOrderMessage *temp = strategy.analyze_market_and_make_decisions();
        if (temp) {
            InOUCHMessages.push_back(temp); // FOR BACKTESTING
            server.sendMessage(temp, sizeof(*temp));     // NO backtesting

        }
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
    }
}


void OrderBookThreadFunction()
{
    // TODO on
    while (true) {
        if (!ITCHQueue.empty()) {
            ITCH::ITCHMessage* msg = ITCHQueue.front();
            ITCHQueue.pop();

            if (msg->type == 'F') {
                ITCH::AddOrderMessageWithMPID* add_order_msg = static_cast<ITCH::AddOrderMessageWithMPID*>(msg);
                std::cout << "TRYING TO ADD: : " << add_order_msg->order_reference_number << std::endl;
                books.add_order(*add_order_msg);
                books.print_all_order_books();
            }
            else if (msg->type == 'E') {
                ITCH::OrderExecutedMessage* executed_order_msg = static_cast<ITCH::OrderExecutedMessage*>(msg);
                books.execute_order(*executed_order_msg);
            }
            else if (msg->type == 'X') {
                ITCH::OrderCancelMessage* cancel_order_msg = static_cast<ITCH::OrderCancelMessage*>(msg);
                std::cout << "TRYING TO CANCEL: : " << cancel_order_msg->order_reference_number << std::endl;
                books.cancel_order(*cancel_order_msg);
                books.print_all_order_books();
            }
            else if (msg->type == 'D') {
                ITCH::OrderDeleteMessage* delete_order_msg = static_cast<ITCH::OrderDeleteMessage*>(msg);
                books.delete_order(*delete_order_msg);
            }
            else if (msg->type == 'U') {
                ITCH::OrderReplaceMessage* replace_order_msg = static_cast<ITCH::OrderReplaceMessage*>(msg);
                books.replace_order(*replace_order_msg);
            }
            // Add handling for other ITCH message types if needed

            delete msg;
        }
    }
}


int main()
{
    TradingStrategy strategy(books);
    std::thread ITCHThread(ITCHthreadFunction);
    std::thread OUCHThread(OutboundOUCHthreadFunction);
    std::thread MarketAnalysisThread(MarketAnalysisThreadFunction, std::ref(strategy));
    std::thread OrderBookThread(OrderBookThreadFunction);

    // Sleep for 10 seconds
    std::this_thread::sleep_for(std::chrono::seconds(15));

    // Close all threads after 10 seconds
    if (ITCHThread.joinable()) {
        ITCHThread.detach();
    }
    if (OUCHThread.joinable()) {
        OUCHThread.detach();
    }
    if (MarketAnalysisThread.joinable()) {
        MarketAnalysisThread.detach();
    }
    if (OrderBookThread.joinable()) {
        OrderBookThread.detach();
    }


    // Perform backtesting after all threads are closed
    Backtester backtester(books, InOUCHMessages);
    backtester.run_backtest(); 

    for (InboundOUCH::EnterOrderMessage* message : InOUCHMessages) {
        delete message;
    }

    return 0;
}


void sendExampleInboundOUCH(OUCHServer& server)
{

    InboundOUCH::EnterOrderMessage *enter_order_message = new InboundOUCH::EnterOrderMessage;
    enter_order_message->type = 'O'; // Enter Order message type
    char order_token[14];
    snprintf(order_token, sizeof(order_token), "%013d", rand());
    order_token[13] = '\0'; // Add null terminator to the 14th character
    // Copy the order token to the EnterOrderMessage struct
    strncpy((char*)enter_order_message->order_token, order_token, 14);
    enter_order_message->buy_sell_indicator = 'B'; // Buy order
    enter_order_message->shares = 100; // Number of shares to buy
    strncpy((char*)enter_order_message->stock, "AMZN\0\0\0\0", 8); // Stock symbol (left-justified, padded with spaces)
    enter_order_message->price = 420; // Price in integer format (e.g., 123456 for $12.3456)
    enter_order_message->time_in_force = 30000; // Time in force (in seconds, e.g., 30 seconds)
    strncpy((char*)enter_order_message->firm, "ABC\0", 4); // Firm identifier
    enter_order_message->display = 'Y'; // Displayed order
    enter_order_message->capacity = 'A'; // Agency capacity
    enter_order_message->intermarket_sweep_eligibility = 'Y'; // Intermarket sweep eligible
    enter_order_message->minimum_quantity = 1; // Minimum quantity
    enter_order_message->cross_type = 'O'; // Not a cross order
    enter_order_message->customer_type = 'I'; // Customer type (e.g., retail)

    server.sendMessage(enter_order_message, sizeof(*enter_order_message));

    InboundOUCH::EnterOrderMessage enter_order;
    enter_order.type = 'O';
    memcpy(enter_order.firm, "ABD\0", 4);
    enter_order.display = 'Y';
    enter_order.capacity = 'A';
    enter_order.intermarket_sweep_eligibility = 'Y';
    enter_order.cross_type = 'O';
    enter_order.customer_type = 'I';

    InboundOUCH::CancelOrderMessage cancel_order;
    cancel_order.type = 'X';
    memcpy(cancel_order.mpid, "ABD\0", 4);
    cancel_order.shares = 100;

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis_buy_sell(0, 1);
    std::uniform_int_distribution<> dis_shares(100, 1000);
    std::uniform_int_distribution<> dis_price(1000, 2000);
    std::uniform_int_distribution<> dis_time_in_force(1, 5);
    std::uniform_int_distribution<> dis_minimum_quantity(50, 500);

    for (int i = 0; i < 500; i++) {
        if (dis_buy_sell(gen) == 0) {
            enter_order.buy_sell_indicator = 'B';
        }
        else {
            enter_order.buy_sell_indicator = 'S';
        }

        char stock_sym[8] = { 'A', 'A', 'P', 'L', '\0', '\0', '\0', '\0' };
        memcpy(enter_order.stock, stock_sym, 8);

        char order_token[14];
        sprintf(order_token, "%010d%03d", i, dis_buy_sell(gen));
        memcpy(enter_order.order_token, order_token, 14);

        enter_order.shares = dis_shares(gen);
        enter_order.price = dis_price(gen);
        enter_order.time_in_force = dis_time_in_force(gen);
        enter_order.minimum_quantity = dis_minimum_quantity(gen);

        server.sendMessage(&enter_order, sizeof(enter_order));

        if (i > 0 && i % 2 == 0) {
            memcpy(cancel_order.order_token, enter_order.order_token, 14);
            server.sendMessage(&cancel_order, sizeof(cancel_order));
        }
    }




}



