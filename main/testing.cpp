#include <cstdlib>
#include <iostream>
#include <nasdaq_itch.h>
// #include <nasdaq_ouch_inbound.h>
#include <stdio.h>
#include <fstream>
#include <ios>
#include <bitset>
#include <queue>
#include <stdint.h>
#include <cstdint>
#include <sys/types.h>
#include <byteswap.h>
#include <ext/stdio_filebuf.h>
#include "printing/printMessage.h"
// #include "matplotlibcpp.h"
using namespace ITCH;

uint64_t bswap_48(uint64_t x);
int main(int argc, char *argv[])
{
    std::queue<ITCHMessage*> q;
    std::ifstream file;
    const char* filename = "Test";
    file.open(filename, std::ios::in | std::ios::binary);
    unsigned char buff;
    int count = 0;

    // First, read next byte, then determine message type
    while(file)
    {
        file.read((char*)&buff, 1);
        // std::cout << "buff: " << std::bitset<8>(buff) << " " << buff << std::endl;
        count += 1;

        if (count >= 1000000)
            break;
            
        if (file.eof())
            break;

        switch(buff) {

            case 'S':
            {
                SystemEventMessage *a = new SystemEventMessage();

                file.unget();
                file.read((char*)a, sizeof(*a));
                // file.read((char*)a, sizes::SystemEventMessageSize);
                a->stock_locate = bswap_16(a->stock_locate);
                a->stock_tracking = bswap_16(a->stock_tracking);
                // a->timestamp = bswap_48(a->timestamp);
                std::cerr << "SystemEventMessage: " << a->event_code << std::endl;
                std::cerr << "stock locate: " << a->stock_locate << std::endl;
                q.push(a);
                break;
            }
            case 'R':
            {
                file.seekg(sizes::StockDirectoryMessageSize, std::ios::cur);
                break;
            }
            case 'H':
            {
                file.seekg(sizes::StockTradingActionMessageSize, std::ios::cur);
                break;
            }
            case 'Y':
            {
                file.seekg(sizes::RegShoRestrictionMessageSize, std::ios::cur);
                break;
            }
            case 'L':
            {
                file.seekg(sizes::MarketParticipantPositionMessageSize, std::ios::cur);
                break;
            }
            case 'V':
            {
                file.seekg(sizes::MarketWideCircuitBreakerDeclineLevelMessageSize, std::ios::cur);
                break;
            }
            case 'W':
            {
                file.seekg(sizes::MarketWideCircuitBreakerStatusMessageSize, std::ios::cur);
                break;
            }
            case 'K':
            {
                file.seekg(sizes::IPOQuotingPeriodUpdateMessageSize, std::ios::cur);
                break;
            }
            case 'J':
            {
                file.seekg(sizes::LULDAuctionCollarMessageSize, std::ios::cur);
                break;
            }
            case 'h':
            {
                file.seekg(sizes::OperationalHaltMessageSize, std::ios::cur);
                break;
            }
            case 'A':
            {
                file.seekg(sizes::AddOrderMessageSize, std::ios::cur);
                break;
            }
            case 'F':
            {
                AddOrderMessageWithMPID *msg = new AddOrderMessageWithMPID();
                file.unget();
                file.read((char*)msg, sizeof(*msg));
                msg->stock_locate = bswap_16(msg->stock_locate);
                msg->stock_tracking = bswap_16(msg->stock_tracking);
                // msg->timestamp = bswap_48(msg->timestamp);
                msg->order_reference_number = bswap_64(msg->order_reference_number);
                msg->shares = bswap_32(msg->shares);
                msg->stock = bswap_64(msg->stock);
                msg->price = bswap_32(msg->price);
                msg->attribution = bswap_32(msg->attribution);
                q.push(msg);
                break;
            }
            case 'C':
            {
                file.seekg(sizes::OrderExecutedWithPriceMessageSize, std::ios::cur);
                break;
            }
            case 'E':
            {
                OrderExecutedMessage *msg = new OrderExecutedMessage();
                file.unget();
                file.read((char*)msg, sizeof(*msg));
                msg->stock_locate = bswap_16(msg->stock_locate);
                msg->stock_tracking = bswap_16(msg->stock_tracking);
                // msg->timestamp = bswap_48(msg->timestamp);
                msg->order_reference_number = bswap_64(msg->order_reference_number);
                msg->executed_shares = bswap_32(msg->executed_shares);
                q.push(msg);
                break;
            }
            case 'X':
            {
                OrderCancelMessage *msg = new OrderCancelMessage();
                file.unget();
                file.read((char*)msg, sizeof(*msg));
                msg->stock_locate = bswap_16(msg->stock_locate);
                msg->stock_tracking = bswap_16(msg->stock_tracking);
                // msg->timestamp = bswap_48(msg->timestamp);
                msg->order_reference_number = bswap_64(msg->order_reference_number);
                msg->cancelled_shares = bswap_32(msg->cancelled_shares);
                q.push(msg);
                break;
            }
            case 'D':
            {
                OrderDeleteMessage *msg = new OrderDeleteMessage();
                file.unget();
                file.read((char*)msg, sizeof(*msg));
                msg->stock_locate = bswap_16(msg->stock_locate);
                msg->stock_tracking = bswap_16(msg->stock_tracking);
                // msg->timestamp = bswap_48(msg->timestamp);
                msg->order_reference_number = bswap_64(msg->order_reference_number);
                q.push(msg);
                break;
            }
            case 'U':
            {
                OrderReplaceMessage *msg = new OrderReplaceMessage();
                file.unget();
                file.read((char*)msg, sizeof(*msg));
                msg->stock_locate = bswap_16(msg->stock_locate);
                msg->stock_tracking = bswap_16(msg->stock_tracking);
                // msg->timestamp = bswap_48(msg->timestamp);
                msg->order_reference_number = bswap_64(msg->order_reference_number);
                msg->new_order_reference_number = bswap_64(msg->new_order_reference_number);
                msg->shares = bswap_32(msg->shares);
                msg->price = bswap_32(msg->price);
                q.push(msg);
                break;
            }
            case 'P':
            {
                file.seekg(sizes::TradeMessageSize, std::ios::cur);
                break;
            }
            case 'Q':
            {
                file.seekg(sizes::CrossTradeMessageSize, std::ios::cur);
                break;
            }
            case 'B':
            {
                file.seekg(sizes::BrokenTradeMessageSize, std::ios::cur);
                break;
            }
            case 'I':
            {
                file.seekg(sizes::NetOrderImbalanceIndicatorMessageSize, std::ios::cur);
                break;
            }
            case 'N':
            {
                file.seekg(sizes::RetailPriceImprovementIndicatorMessageSize, std::ios::cur);
                break;
            }
    }



    }
    

    std::cerr << "Done" << q.size() << std::endl;
    for(size_t i = 0; i <= q.size(); i++)
    { 
        ITCHMessage *temp = q.front();
        printCorrectMessage(temp);
        q.pop();
        
    }

    file.close();
 

    
    // tp.~tcp_client();
    // tp.close_fd();
    // std::cerr << "Shit " << q.size() << " " << std::endl;
    // std::cerr << "After Done " << q.size() << " " << std::endl;
    // for(size_t i = 0; i <= q.size(); i++)
    // { 
    //     std::cerr << "i " << i << std::endl;
    //     ITCH::ITCHMessage *e = (ITCH::ITCHMessage *)q.front();
    //     std::cerr << e->type << std::endl;
    //     // std::cout << std::endl << e->type << std::endl;
    //     printMessage(e);
    //     // // std::cout << std::bitset<96>(q.front()) << std::endl;
    //     // printMessage(q.front());
    //     q.pop();
        
    // }
    // file.close();
 
    return 0;
}


uint64_t bswap_48(uint64_t x) 
{
    return ((((x) & 0x0000000000ff) << 40) |    
    (((x) & 0x00000000ff00) << 24) |    
    (((x) & 0x000000ff0000) << 8)  |    
    (((x) & 0x0000ff000000) >> 8)  |    
    (((x) & 0x00ff00000000) >> 24) |    
    (((x) & 0xff0000000000) >> 40));
}
